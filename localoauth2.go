package localoauth2

import (
  "github.com/ghodss/yaml"
  "github.com/skratchdot/open-golang/open"
  "golang.org/x/oauth2"
  "io/ioutil"
  "log"
  "net/http"
  "net/url"
  "math/rand"
  "strconv"
)

type oauthConfig struct {
  Token    *oauth2.Token
  Config   oauth2.Config
}

func (c oauthConfig) writeTo(fileName string) {
  js, _ := yaml.Marshal(c)

  err := ioutil.WriteFile(fileName, js, 0600)
  if err != nil {
    log.Fatal(err)
  }
}

func (c *oauthConfig) readFrom(fileName string) {
  f, ferr := ioutil.ReadFile(fileName)
  if ferr != nil {
    log.Fatal(ferr)
  }

  jserr := yaml.Unmarshal(f, c)
  if jserr != nil {
    log.Fatal(jserr)
  }

}

func listenForCallback(callback string, state string, c chan string) {
  cburl, cberr := url.Parse(callback)
  if cberr != nil {
    log.Fatal(cberr)
  }

  http.HandleFunc(cburl.EscapedPath(), func(w http.ResponseWriter, r *http.Request) {

    params, err := url.ParseQuery(r.URL.RawQuery)
    if err != nil {
      log.Fatal(err)
    }

    if st, ok := params["state"]; ok && st[0] != state {
      log.Fatal("Did not get back a valid state", r.URL.String())
    }

    if code, ok := params["code"]; ok {
      c <- code[0]
    } else {
      log.Fatal("Did not get back a valid code", r.URL.String())
    }
  })

  log.Fatal(http.ListenAndServe(cburl.Host, nil))
}

// Gets a net/http.Client object that can work with OAuth2 REST apis.
func GetClient(configFile string) *http.Client {
  var conf oauthConfig
  conf.readFrom(configFile)

  // If there is no Token then make a request to go authorize
  // for that token.
  if conf.Token == nil || conf.Token.AccessToken == "" {
    state := strconv.Itoa(rand.Int())
    requests := make(chan string)
    go listenForCallback(conf.Config.RedirectURL, state, requests)

    // Redirect user to consent page to ask for permission
    // for the scopes specified above.
    url := conf.Config.AuthCodeURL(state, oauth2.AccessTypeOnline)
    open.Run(url)

    code := <-requests
    tok, err := conf.Config.Exchange(oauth2.NoContext, code)
    if err != nil {
      log.Fatal(err)
    }
    conf.Token = tok

    conf.writeTo(configFile)
  }

  return conf.Config.Client(oauth2.NoContext, conf.Token)
}
