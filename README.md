# Local OAuth2 for Go

Helper package for getting access tokens for browserless applications.

# Installation

~~~~
go get bitbucket.org/jeffywu/localoauth2
~~~~

# Example

First create a yaml file that holds configuration for your OAuth2 application. It should look something like this:

    Config:
      ClientID: <YOUR CLIENT ID>
      ClientSecret: <YOUR CLIENT SECRET>
      Endpoint:
        AuthURL: <APPLICATION AUTH URL>
        TokenURL: <APPLICATION TOKEN URL>
      RedirectURL: http://localhost:3000/example/callback
      Scopes:
      - A
      - List
      - Of
      - Scopes


Then you can easily get an [oauth2](https://github.com/golang/oauth2) Config and Token object by running the following code:

    import "bitbucket.org/jeffywu/localoauth2"

    client := localoauth2.GetClient("app.yaml")
    resp, err := client.Get("http://example.org/rest")
